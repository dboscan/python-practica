from __future__ import unicode_literals

import datetime
from django.db import models
from django.utils import timezone


# Create your models here.


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField("Date published")  # Colocar un nombre para que se pueda ser agarrado por Django mas amigable

    #  La siguiente funcion hara que al pedir el obj me retorne este string
    def __str__(self):
        return self.question_text

    # Funcion normal para demostrar funcionamiento
    def dias_publicado(self):
        return self.pub_date >= timezone.now()


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)  # Valor obligatorio de maximo cuantos caracteres
    votes = models.IntegerField(default=0)  # Default es un valor opcional en los Integer

    def __str__(self):
        return self.choice_text
