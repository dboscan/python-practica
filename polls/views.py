from django.shortcuts import render, get_object_or_404, reverse
from django.http import HttpResponse, HttpResponseRedirect
from .models import Question, Choice
from django.views import generic
from django.template import loader

# Create your views here.


# # Retornar las ultimas 5 preguntas creadas
# def index(request):
#     questions = Question.objects.order_by("-pub_date")[:5]  # Recordar las rebanadas [0:5]
#     # output = ", ".join([q.question_text for q in questions])
#
#     # Cargar template con loader (importada de django.template)
#     # template = loader.get_template("polls/index.html")
#
#     # Establecer el contexto con un diccionario
#     context = {
#         "list_questions": questions,
#     }
#     return render(request, "polls/index.html", context)
#
#
# def detail(request, question_id):
#     # try:
#     #     question = Question.objects.get(id=question_id)
#     # except Question.DoesNotExist:
#     #     raise Http404("Question doesn't exists")
#     # return render(request, "polls/detail.html", {"question": question})
#
#     question = get_object_or_404(Question, id=question_id)
#     return render(request, "polls/detail.html", {"question": question})
#
#
# def results(request, question_id):
#     question = get_object_or_404(Question, pk=question_id)
#     return render(request, "polls/results.html", {"question": question})

class IndexView (generic.ListView):
    template_name = "polls/index.html"
    context_object_name = "list_questions"

    def get_queryset(self):
        return Question.objects.order_by("-pub_date")[:5]


class DetailView (generic.DetailView):
    model = Question
    template_name = "polls/detail.html"


class ResultsView(generic.DetailView):
    # DetailView espera un parametro por URL con nombre pk
    model = Question  # Preguntar esto
    template_name = "polls/results.html"


class ResultView2(generic.TemplateView):
    template_name = "polls/results.html"

    def get_context_data(self, pk, **kwargs):
        context = super(ResultView2,self).get_context_data(**kwargs)
        question = get_object_or_404(Question, pk=pk)
        context.update({
            "question": question,
        })
        return context


def vote(request, question_id):
    # Recibo el id de la pregunta por la URL
    question = get_object_or_404(Question, pk=question_id)
    try:
        choice = question.choice_set.get(pk=request.POST["choice"])
    except (KeyError, Choice.DoesNotExist):
        # Volvemos a mostrar el form
        return render(request, "poll/detail.html", {
            "question": choice,
            "error_message": "No seleccionaste una respuesta valida"
        })
    else:
        choice.votes += 1
        choice.save()
        # Luego de cada POST, SIEMPRE tenemos que enviar un HTTPResponseRedirect
        return HttpResponseRedirect(reverse("polls:results", args=(question_id,)))
